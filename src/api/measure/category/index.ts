import request from '@/config/axios'

// Measure 类别 VO
export interface CategoryVO {
  id: number // 编号
  name: string // 类别名称
  remark: string // 备注
}

// Measure 类别 API
export const CategoryApi = {
  // 查询Measure 类别分页
  getCategoryPage: async (params: any) => {
    return await request.get({ url: `/measure/category/page`, params })
  },

  // 查询Measure 类别详情
  getCategory: async (id: number) => {
    return await request.get({ url: `/measure/category/get?id=` + id })
  },
  
  // 查询类别精简列表
  getCategorySimpleList: async () => {
    return await request.get({ url: `/measure/category/simple-list` })
  },

  // 新增Measure 类别
  createCategory: async (data: CategoryVO) => {
    return await request.post({ url: `/measure/category/create`, data })
  },

  // 修改Measure 类别
  updateCategory: async (data: CategoryVO) => {
    return await request.put({ url: `/measure/category/update`, data })
  },

  // 删除Measure 类别
  deleteCategory: async (id: number) => {
    return await request.delete({ url: `/measure/category/delete?id=` + id })
  },

  // 导出Measure 类别 Excel
  exportCategory: async (params) => {
    return await request.download({ url: `/measure/category/export-excel`, params })
  },

// ==================== 子表（Measure 类别项） ====================

  // 获得Measure 类别项分页
  getCategoryItemPage: async (params) => {
    return await request.get({ url: `/measure/category/category-item/page`, params })
  },

  // 新增Measure 类别项
  createCategoryItem: async (data) => {
    return await request.post({ url: `/measure/category/category-item/create`, data })
  },

  // 修改Measure 类别项
  updateCategoryItem: async (data) => {
    return await request.put({ url: `/measure/category/category-item/update`, data })
  },

  // 删除Measure 类别项
  deleteCategoryItem: async (id: number) => {
    return await request.delete({ url: `/measure/category/category-item/delete?id=` + id })
  },

  // 获得Measure 类别项
  getCategoryItem: async (id: number) => {
    return await request.get({ url: `/measure/category/category-item/get?id=` + id })
  },
}
