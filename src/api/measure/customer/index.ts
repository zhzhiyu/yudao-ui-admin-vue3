import request from '@/config/axios'

// Measure 客户别 VO
export interface CustomerVO {
  id: number // 编号
  name: string // 客户别名称
  remark: string // 备注
}

// Measure 客户别 API
export const CustomerApi = {
  // 查询Measure 客户别分页
  getCustomerPage: async (params: any) => {
    return await request.get({ url: `/measure/customer/page`, params })
  },

  // 查询Measure 客户别详情
  getCustomer: async (id: number) => {
    return await request.get({ url: `/measure/customer/get?id=` + id })
  },
  
  // 查询客户精简列表
  getCustomerSimpleList: async () => {
    return await request.get({ url: `/measure/customer/simple-list` })
  },

  // 新增Measure 客户别
  createCustomer: async (data: CustomerVO) => {
    return await request.post({ url: `/measure/customer/create`, data })
  },

  // 修改Measure 客户别
  updateCustomer: async (data: CustomerVO) => {
    return await request.put({ url: `/measure/customer/update`, data })
  },

  // 删除Measure 客户别
  deleteCustomer: async (id: number) => {
    return await request.delete({ url: `/measure/customer/delete?id=` + id })
  },

  // 导出Measure 客户别 Excel
  exportCustomer: async (params) => {
    return await request.download({ url: `/measure/customer/export-excel`, params })
  },
}