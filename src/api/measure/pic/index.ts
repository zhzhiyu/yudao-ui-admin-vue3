import request from '@/config/axios'

// Measure 图像 VO
export interface PicVO {
  id: number // 编号
  clientId: number  // 客户端编号
  categoryId: number  // 类别编号
  picUrl: string  // 图像地址
  magnification: number  // 放大倍率
  measureTaskId: number  // 测量任务编号, 默认0
}

export interface BindToTaskVO {
  picIds: number[] // 编号列表
  measureTaskId: number  // 测量任务编号
}

// Measure 图像 API
export const PicApi = {
  // 查询Measure 图像分页
  getPicPage: async (params: any) => {
    return await request.get({ url: `/measure/pic/page`, params })
  },

  // 查询Measure 图像详情
  getPic: async (id: number) => {
    return await request.get({ url: `/measure/pic/get?id=` + id })
  },
  
  // 查询图像精简列表
  getPicSimpleList: async (taskId: number) => {
    return await request.get({ url: `/measure/pic/simple-list?taskId=` + taskId })
  },

  // 新增Measure 图像
  createPic: async (data: PicVO) => {
    return await request.post({ url: `/measure/pic/create`, data })
  },

  // 修改Measure 图像
  updatePic: async (data: PicVO) => {
    return await request.put({ url: `/measure/pic/update`, data })
  },

  // 删除Measure 图像
  deletePic: async (id: number) => {
    return await request.delete({ url: `/measure/pic/delete?id=` + id })
  },

  // 将图像列表绑定到指定任务
  bindToTask: async (data: BindToTaskVO) => {
    return await request.post({ url: `/measure/pic/bind-to-task`, data })
  },
}