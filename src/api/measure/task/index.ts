import request from '@/config/axios'

// Measure 任务 VO
export interface TaskVO {
  id: number // 编号
  lotNo: string // LOT号
  customerId: number // 客户别编号
  categoryId: number // 类别编号
  magnification: number // 倍率
  count: number // 数量
  status: number // 任务状态
  clientId: number // 客户端编号
}

export interface TaskBindVO {
  taskId: number // 任务编号
  picList: number[]  // 图像编号列表
}

// Measure 任务结果 VO
export interface TaskResultVO {
  id: number // 编号
  no: string  // 任务单号
  lotNo: string // LOT号
  componentName: string // 品目名
  customerId: number // 客户别编号
  customerName: string // 客户别名称
  categoryId: number // 类别编号
  categoryName: string // 类别名称
  clientId: number // 客户端编号
  clientName: string // 客户端别名
  magnification: number // 倍率
  count: number // 数量
  status: number // 任务状态
  finalResult: number // 最终判定
  remark: string // 备注
  creatorName: string // 担当者(创建人)
  createTime: Date // 任务创建日期
  categoryItemList: categoryItemRespVO[] // 测量项目规格值列表
  picList: PicResult[]  // 图像及其测量结果列表
  miscResultRespVOList: MeasureMiscResultRespVO[] // 杂项判定列表
}

export interface categoryItemRespVO {
  id: number // 编号
  name: string // 测量位置名称
  description: string  // 描述
  stdRange: string // 规格值"20,30"
  limitRange: string // 管理界限值"22,27"
}

export interface PicResult {
  id: number // 测量项目编号
  picUrl: string // 图像地址
  coreResultRespVOList: MeasureCoreResultRespVO[]  // 测量结果列表
  picResult: number // 图像多个测结果的综合判定
}

export interface MeasureCoreResultRespVO {
  id: number
  categoryItemId: number // 类别项的测量位置编号
  measureItemValue: string // 测量位置结果值(float)
  startPoint: string // 测量值坐标起点
  endPoint: string // 测量值坐标终点
  coreResult: number // 判定结果
  createTime: Date
}

export interface MeasureMiscResultRespVO {
  id: number
  measureTaskId: number // 测量任务编号
  miscResultItemId: number // 判定项字典编号
  miscResultItemName: string // 判定项目名称
  miscResult: number // 杂项判定结果
}

// Measure 任务 API
export const TaskApi = {
  // 查询Measure 任务分页
  getTaskPage: async (params: any) => {
    return await request.get({ url: `/measure/task/page`, params })
  },

  // 查询Measure 任务详情
  getTask: async (id: number) => {
    return await request.get({ url: `/measure/task/get?id=` + id })
  },

  // 新增Measure 任务
  createTask: async (data: TaskVO) => {
    return await request.post({ url: `/measure/task/create`, data })
  },

  // 修改Measure 任务
  updateTask: async (data: TaskVO) => {
    return await request.put({ url: `/measure/task/update`, data })
  },
  
  // 发起分析测量任务
  requestAnalysis: async (id: number) => {
    return await request.put({ url: `/measure/task/request-analysis?id=` + id})
  },
  
  // 查询测量结果
  getTaskResult: async (id: number) => {
    return await request.get({ url: `/measure/task/result?taskId=` + id})
  },

  // 删除Measure 任务
  deleteTask: async (id: number) => {
    return await request.delete({ url: `/measure/task/delete?id=` + id })
  },

  // 导出Measure 任务 Excel
  exportTask: async (params) => {
    return await request.download({ url: `/measure/task/export-excel`, params })
  }
}
