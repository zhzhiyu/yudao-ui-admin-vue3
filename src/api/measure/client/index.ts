import request from '@/config/axios'

// Measure 显微镜客户端 VO
export interface ClientVO {
  id: number // 编号
  name: string // 客户端别名
  status: number // 客户端状态
  ipAddress: string // 客户端IP地址
  status: number // 在线状态
}

// Measure 显微镜客户端 API
export const ClientApi = {
  // 查询Measure 显微镜客户端分页
  getClientPage: async (params: any) => {
    return await request.get({ url: `/measure/client/page`, params })
  },

  // 查询Measure 显微镜客户端详情
  getClient: async (id: number) => {
    return await request.get({ url: `/measure/client/get?id=` + id })
  },
  
  // 查询显微镜客户端精简列表
  getClientSimpleList: async () => {
    return await request.get({ url: `/measure/client/simple-list` })
  },

  // 新增Measure 显微镜客户端
  createClient: async (data: ClientVO) => {
    return await request.post({ url: `/measure/client/create`, data })
  },

  // 修改Measure 显微镜客户端
  updateClient: async (data: ClientVO) => {
    return await request.put({ url: `/measure/client/update`, data })
  },

  // 删除Measure 显微镜客户端
  deleteClient: async (id: number) => {
    return await request.delete({ url: `/measure/client/delete?id=` + id })
  },

  // 导出Measure 显微镜客户端 Excel
  exportClient: async (params) => {
    return await request.download({ url: `/measure/client/export-excel`, params })
  },
}