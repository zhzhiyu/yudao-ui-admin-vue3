import request from '@/config/axios'

// Measure 判定项目 VO
export interface MiscResultItemVO {
  id: number // 编号
  name: string // 判定项目名称
  remark: string // 备注
}

// Measure 判定项目 API
export const MiscResultItemApi = {
  // 查询Measure 判定项目分页
  getMiscResultItemPage: async (params: any) => {
    return await request.get({ url: `/measure/misc-result-item/page`, params })
  },

  // 查询Measure 判定项目详情
  getMiscResultItem: async (id: number) => {
    return await request.get({ url: `/measure/misc-result-item/get?id=` + id })
  },
  
  // 查询判定项目精简列表
  getMiscResultItemList: async () => {
    return await request.get({ url: `/measure/misc-result-item/list` })
  },

  // 新增Measure 判定项目
  createMiscResultItem: async (data: MiscResultItemVO) => {
    return await request.post({ url: `/measure/misc-result-item/create`, data })
  },

  // 修改Measure 判定项目
  updateMiscResultItem: async (data: MiscResultItemVO) => {
    return await request.put({ url: `/measure/misc-result-item/update`, data })
  },

  // 删除Measure 判定项目
  deleteMiscResultItem: async (id: number) => {
    return await request.delete({ url: `/measure/misc-result-item/delete?id=` + id })
  },

}
