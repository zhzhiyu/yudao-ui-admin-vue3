import request from '@/config/axios'

// Measure 判定项目 VO
export interface MiscResultSaveReqVO {
  measureTaskId: number // 测量任务编号
  remark: string // 备注
  miscResultList: MiscResult[] // 判定结果列表
}

export interface MiscResult {
  id: number // 判定结果编号
  miscResultItemId: number // 判定项目编号
  miscResult: number // 判定结果
}

// Measure 判定项目 API
export const MiscResultApi = {
  // 修改Measure 判定结果
  updateMiscResult: async (data: MiscResultSaveReqVO) => {
    return await request.put({ url: `/measure/misc-result/update`, data })
  },
}
