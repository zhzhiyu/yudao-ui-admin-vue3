import request from '@/config/axios'

// ERP 其它收款单 VO
export interface FinanceOtherReceiptVO {
  id: number // 收款单编号
  no: string // 收款单号
  customerId: number // 客户编号
  receiptTime: Date // 收款时间
  totalPrice: number // 合计金额，单位：元
  status: number // 状态
  remark: string // 备注
}

// ERP 收款单 API
export const FinanceOtherReceiptApi = {
  // 查询收款单分页
  getFinanceOtherReceiptPage: async (params: any) => {
    return await request.get({ url: `/erp/finance-other-receipt/page`, params })
  },

  // 查询收款单详情
  getFinanceOtherReceipt: async (id: number) => {
    return await request.get({ url: `/erp/finance-other-receipt/get?id=` + id })
  },

  // 新增收款单
  createFinanceOtherReceipt: async (data: FinanceOtherReceiptVO) => {
    return await request.post({ url: `/erp/finance-other-receipt/create`, data })
  },

  // 修改收款单
  updateFinanceOtherReceipt: async (data: FinanceOtherReceiptVO) => {
    return await request.put({ url: `/erp/finance-other-receipt/update`, data })
  },

  // 更新收款单的状态
  updateFinanceOtherReceiptStatus: async (id: number, status: number) => {
    return await request.put({
      url: `/erp/finance-other-receipt/update-status`,
      params: {
        id,
        status
      }
    })
  },

  // 删除收款单
  deleteFinanceOtherReceipt: async (ids: number[]) => {
    return await request.delete({
      url: `/erp/finance-other-receipt/delete`,
      params: {
        ids: ids.join(',')
      }
    })
  },

  // 导出收款单 Excel
  exportFinanceOtherReceipt: async (params: any) => {
    return await request.download({ url: `/erp/finance-other-receipt/export-excel`, params })
  }
}
