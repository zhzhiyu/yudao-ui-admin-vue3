import request from '@/config/axios'

// ERP 销售订单 VO
export interface ShengquSaleOrderVO {
  id: number // 订单工单编号
  no: string // 销售订单号
  requestOrderNo: string // 请求订单的单号
  targetAccount: string // 目标充值账户
  customerId: number // 客户编号
  orderTime: Date // 订单时间
  totalCount: number // 合计数量
  totalPrice: number // 合计金额，单位：元
  result: number // 充值结果
  remark: string // 备注
  outCount: number // 销售出库数量
  returnCount: number // 销售退货数量
}

// ERP 销售订单 API
export const ShengquSaleOrderApi = {
  // 查询销售订单分页
  getSaleOrderPage: async (params: any) => {
    return await request.get({ url: `/erp/shengqu-sale-order/page`, params })
  },

  // 查询销售订单详情
  getSaleOrder: async (id: number) => {
    return await request.get({ url: `/erp/shengqu-sale-order/get?id=` + id })
  },

  // 新增销售订单
  createSaleOrder: async (data: ShengquSaleOrderVO) => {
    return await request.post({ url: `/erp/shengqu-sale-order/create`, data })
  },

  // 修改销售订单
  updateSaleOrder: async (data: ShengquSaleOrderVO) => {
    return await request.put({ url: `/erp/shengqu-sale-order/update`, data })
  },

  // 更新销售订单的状态
  updateSaleOrderStatus: async (id: number, status: number) => {
    return await request.put({
      url: `/erp/shengqu-sale-order/update-status`,
      params: {
        id,
        status
      }
    })
  },

  // 删除销售订单
  deleteSaleOrder: async (ids: number[]) => {
    return await request.delete({
      url: `/erp/shengqu-sale-order/delete`,
      params: {
        ids: ids.join(',')
      }
    })
  },

  // 导出销售订单 Excel
  exportSaleOrder: async (params: any) => {
    return await request.download({ url: `/erp/shengqu-sale-order/export-excel`, params })
  },
  
  // 下载订单导入模板
  getImportShengquSaleOrderTemplate: async () => {
    return await request.download({ url: '/erp/shengqu-sale-order/get-import-template' })
  }
}
